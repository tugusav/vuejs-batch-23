//soal 1
var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sorted = daftarBuah.sort()

for (var i = 0; i < sorted.length; i++){
    console.log(sorted[i]);
}
// soal 2

function introduce(datadiri){
    return "Nama saya " + datadiri.name+", umur saya " + datadiri.age +", alamat saya di " + datadiri.address + ", dan saya punya hobby yaitu " + datadiri.hobby
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

//soal 3
function hitung_huruf_vokal(kata){
    var counter = 0;
    var chr = kata.toString().toLowerCase() // mengubah semua huruf menjadi huruf kecil untuk memudahkan if statement
    for(var i = 0; i < chr.length; i++){
        if(chr.charAt(i) == 'a' || chr.charAt(i) == 'i'|| chr.charAt(i) == 'u' || chr.charAt(i) == 'e' || chr.charAt(i) == 'o'){
            counter++
        }
    }
    return counter;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2


// soal 4
function hitung(angka){
    return 2*angka - 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8