// soal 1
var nilai = 85;
var indeks = ''
if (nilai >= 85){
    index = 'A'
    console.log("Indeksnya " + index)
} else if (nilai >= 75 && nilai < 85){
    index = 'B'
    console.log("Indeksnya " + index)
} else if (nilai >=65 && nilai < 75){
    index = 'C'
    console.log("Indeksnya " + index)
} else if (nilai >= 55 && nilai < 65){
    index = 'D'
    console.log("Indeksnya " + index)
} else{ // nilai < 55
    index = 'E'
    console.log("Indeksnya " + index)
}

// soal 2
var tanggal = 14;
var bulan = 4;
var tahun = 2001;

switch (bulan) {
    case 1:{
        console.log(tanggal + ' Januari ' + tahun)
        break
    }
    case 2:{
        console.log(tanggal + ' Februari ' + tahun)
        break
    }
    case 3:{
        console.log(tanggal + ' Maret ' + tahun)
        break
    }
    case 4:{
        console.log(tanggal + ' April ' + tahun)
        break
    }
    case 5:{
        console.log(tanggal + ' Mei ' + tahun)
        break
    }
    case 6:{
        console.log(tanggal + ' Juni ' + tahun)
        break
    }
    case 7:{
        console.log(tanggal + ' Juli ' + tahun)
        break
    }
    case 8:{
        console.log(tanggal + ' Agustus ' + tahun)
        break
    }
    case 9:{
        console.log(tanggal + ' September ' + tahun)
        break
    }
    case 10:{
        console.log(tanggal + ' Oktober ' + tahun)
        break
    }
    case 11:{
        console.log(tanggal + ' November ' + tahun)
        break
    }
    case 12:{
        console.log(tanggal + ' Desember ' + tahun)
        break
    }
    default:
        console.log("Bulan tidak terdefinisi. Harap masukkan bulan dengan range 1-12")
        break;
}
// soal 3

var n = 7
for(var i = 0; i < n; i++) {
    //baris
    var str = ''
   for(var j = 0; j<=i; j++) {
       //kolom
       str += '*'
    }
    console.log(str)
 }

 // soal 4

var m = 10;
var counter = 1;
var one = 'programming';
var two = 'Javascript';
var three = 'VueJS'
var border = ''

while (counter <= m){
    border += '='
    if (counter % 3 == 1){
        console.log(counter + " - "+ "I love "+ one)
    } else if (counter % 3 == 2){
        console.log(counter + " - "+ "I love "+ two)
    } else {
        // counter % 3 == 0
        console.log(counter + " - "+ "I love "+ three)
        console.log(border)
    }
    counter++;
}